﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyMove : MonoBehaviour {

    // ################################## Global Variables ##################################

    // ---------------------------------- Public ----------------------------------
    #region private Serialized
    [SerializeField] private List<GameObject> lstWaypoints = new List<GameObject>();
    [SerializeField] private int iCurrentWaypoint;
    [SerializeField] private float fSpeed;
    [SerializeField] private AnimationCurve acSpeed;
    [SerializeField] private GameObject goBottom;
    #endregion

    // ---------------------------------- Private ----------------------------------
    #region private
    private float fStartTime;
    private float fTimeToNextWaypoint;
    private GameObject goCurrentWaypoint;
    private Vector3 v3StartPositon;
    private float fFrac;
    #endregion


    // ################################## Awake / Start / Update ##################################
    void Start ()
    {
        iCurrentWaypoint = 0;
        goCurrentWaypoint = lstWaypoints[iCurrentWaypoint];

        fTimeToNextWaypoint = Vector3.Distance(transform.position, goCurrentWaypoint.transform.position) / fSpeed;

        fStartTime = Time.time;

        v3StartPositon = transform.position;

    }
	
	void Update ()
    {
	    if (goCurrentWaypoint != null)
        {
            fFrac = (Time.time - fStartTime) / fTimeToNextWaypoint;

            if (fFrac < 1)
            {
                transform.position = Vector3.Lerp(v3StartPositon, goCurrentWaypoint.transform.position, acSpeed.Evaluate(fFrac));

                goBottom.transform.LookAt(goCurrentWaypoint.transform.position);
            }
            else
            {
                iCurrentWaypoint++;
                if (iCurrentWaypoint >= lstWaypoints.Count)
                {
                    iCurrentWaypoint = 0;
                }

                goCurrentWaypoint = lstWaypoints[iCurrentWaypoint];

                fTimeToNextWaypoint = Vector3.Distance(transform.position, goCurrentWaypoint.transform.position) / fSpeed;

                fStartTime = Time.time;

                v3StartPositon = transform.position;
            }
        }
	}

}