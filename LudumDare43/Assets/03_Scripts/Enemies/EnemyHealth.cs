﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyHealth : MonoBehaviour {

    // ################################## Global Variables ##################################

    // ---------------------------------- Public ----------------------------------
    #region Serialized private
    [Header("link")]
    [SerializeField] private GameObject goLaser;
    [SerializeField] private GameObject goUI;
    [SerializeField] private GameObject goBottom;
    [SerializeField] private GameObject goCannon;
    [SerializeField] private GameObject goMovement;
    [SerializeField] private List<GameObject> lstWreckParticles= new List<GameObject>();

    [Header("Explosion Parameters")]
    [SerializeField] float fExplosionUpForceModifier;
    [SerializeField] float fExplosionForce;
    [SerializeField] float fExplosionTorque;
    #endregion

    // ---------------------------------- Private ----------------------------------
    #region private
    private PlayerManager playerManager;
    #endregion


    // ################################## Awake / Start / Update ##################################
    private void Start()
    {
        playerManager = InitialComponents.GetPlayerManager();
    }

    // ################################## Kill Enemy ##################################
    public void Kill()
    {
        playerManager.RemoveTurret(gameObject);

        Destroy(gameObject.GetComponent<EnemyAim>());

        if (goMovement != null)
        {
            Destroy(goMovement.GetComponent<EnemyMove>());
        }

        Destroy(goLaser);

        Destroy(goUI);

        Destroy(goCannon);

        Destroy(goBottom);

        foreach (GameObject goWreck in lstWreckParticles)
        {
            goWreck.SetActive(true);

            Rigidbody rb = goWreck.GetComponent<Rigidbody>();

            if (rb != null)
            {
                rb.angularDrag = 5;

                Vector3 v3ExplosionDirection = (gameObject.transform.parent.transform.position - rb.centerOfMass).normalized;

                v3ExplosionDirection.y = 0;

                rb.AddForce((Vector3.up * fExplosionUpForceModifier + v3ExplosionDirection) * fExplosionForce, ForceMode.Impulse);
                rb.AddTorque(new Vector3(Random.Range(-1, 1), Random.Range(-1, 1), Random.Range(-1, 1)) * fExplosionTorque, ForceMode.Impulse);
            }
        }
    }
	
}