﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyAim : MonoBehaviour {

    // ################################## Global Variables ##################################

    // ---------------------------------- Public ----------------------------------
    #region Serialized Private
    [Header("Enemy Parameters")]
    [SerializeField] private float fTimeBeforeShot;

    [Header("link")]
    [SerializeField] private GameObject goRayStart;
    [SerializeField] private GameObject goCannon;
    [SerializeField] private GameObject goShootStartPos;
    [SerializeField] private GameObject goProjectilePrefab;
    [SerializeField] private GameObject goLaser;
    [SerializeField] private Material materialNoTarget;
    [SerializeField] private Material materialTarget;
    [SerializeField] private EnemyShootBar enemyShootBar;
    #endregion

    // ---------------------------------- Private ----------------------------------
    #region private
    private AudioManager audioManager;

    private GameObject goPlayer;
    private GameObject goTarget;
    private GameObject goProjectile;
    private GameObject goProjectileParent;

    private Ray ray;
    private RaycastHit raycastHit;

    private LayerMask layerMaskObstacles;

    private float fTimeStartSeeingPlayer;

    private bool blnPlayerInSight;

    #endregion


    // ################################## Awake / Start / Update ##################################
    private void Start ()
    {
        audioManager = InitialComponents.GetAudioManager();

        goProjectileParent = InitialComponents.GetProjectileParent();

        layerMaskObstacles = 1 << 10; // Obstacles
        layerMaskObstacles |= 1 << 11; // Player

    }

    private void Update()
    {
        if (goTarget != null)
        {

            ray = new Ray(goRayStart.transform.position, goTarget.transform.position - goRayStart.transform.position);

            if (Physics.Raycast(ray, out raycastHit, layerMaskObstacles))
            {

                if (raycastHit.transform.gameObject == goPlayer)
                {
                    Vector3 v3LaserPosition = goRayStart.transform.position;

                    goLaser.transform.position = v3LaserPosition;

                    Vector3 v3NewScale = new Vector3(1,1,1);

                    v3NewScale.z = Vector3.Distance(goRayStart.transform.position, goTarget.transform.position) / 2;

                    goLaser.transform.localScale = v3NewScale;

                    goLaser.GetComponent<Renderer>().material = materialTarget;

                    Vector3 v3LookAtPosition = goTarget.transform.position;
                    v3LookAtPosition.y = 0;

                    goCannon.transform.LookAt(v3LookAtPosition);

                    if (!blnPlayerInSight)
                    {
                        blnPlayerInSight = true;
                        fTimeStartSeeingPlayer = Time.time;

                        enemyShootBar.StartCountDown(fTimeBeforeShot);
                    }
                    else
                    {
                        if (Time.time > fTimeStartSeeingPlayer + fTimeBeforeShot)
                        {
                            Shoot();

                            enemyShootBar.StopCountDown();
                        }
                    }
                }
                else
                {
                    goLaser.GetComponent<Renderer>().material = materialNoTarget;

                    if (blnPlayerInSight)
                    {
                        enemyShootBar.StopCountDown();

                        blnPlayerInSight = false;
                    }
                }

            }
        }
    }

    // ################################## Shoot ##################################
    private void Shoot()
    {
        audioManager.PlayShootingSound(.5f);

        goProjectile = Instantiate(goProjectilePrefab, goShootStartPos.transform.position, Quaternion.identity, goProjectileParent.transform);

        goProjectile.GetComponent<Projectile>().SetupProjectile(goShootStartPos.transform.position, goPlayer);

        goPlayer = null;

        goTarget = null;
    }


    public void SetTarget(GameObject goNewTarget)
    {
        goPlayer = goNewTarget;

        goTarget = goPlayer.GetComponent<PlayerComponents>().goRayTarget;
    }

}