﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyShootBar : MonoBehaviour {

    // ################################## Global Variables ##################################

    // ---------------------------------- Classes ----------------------------------
    #region classes

    #endregion

    // ---------------------------------- Public ----------------------------------
    #region Serialized private
    [SerializeField] private GameObject goWantedPosition;
    [SerializeField] private GameObject goFill;
    #endregion

    // ---------------------------------- Private ----------------------------------
    #region private
    private Camera mainCamera;
    private Vector2 v2FillStartPosition;
    private float fStartTime;
    private float fTimeToFillBar;
    private bool blnFillBar = false;
    #endregion


    // ################################## Awake / Start / Update ##################################
    void Start () {
        mainCamera = InitialComponents.GetMainCamera();

        v2FillStartPosition = goFill.transform.localPosition;

    }
	
	void Update ()
    {
        transform.position = mainCamera.WorldToScreenPoint(goWantedPosition.transform.position);

        if (blnFillBar)
        {
            float fFrac = (Time.time - fStartTime) / fTimeToFillBar;

            goFill.transform.localPosition = new Vector2 (v2FillStartPosition.x + (-v2FillStartPosition.x) * fFrac , 0);
        }
	}
	

    public void StartCountDown(float fTimeToShoot)
    {
        blnFillBar = true;
        fStartTime = Time.time;
        fTimeToFillBar = fTimeToShoot;
    }


    public void StopCountDown()
    {
        blnFillBar = false;
        goFill.transform.localPosition = v2FillStartPosition;
    }
	
	// ################################## Variouse ##################################
	
	// ---------------------------------- Called every frame by Update ----------------------------------
	
}