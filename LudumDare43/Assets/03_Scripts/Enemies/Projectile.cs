﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Projectile : MonoBehaviour {

    // ################################## Global Variables ##################################

    // ---------------------------------- Public ----------------------------------
    #region Serialized Private
    [Header("Projectile Parameters")]
    [SerializeField] private AnimationCurve acShotArc;
    [SerializeField] private float fShotHeight;
    [SerializeField] private float fSpeed;

    [Header("Explosion Parameters")]
    [SerializeField] private float fExplosionUpForceModifier;
    [SerializeField] private float fExplosionForce;
    [SerializeField] private float fExplosionTorque;

    [Header("GameObjects - link")]
    [SerializeField] private GameObject goShell;
    [SerializeField] private GameObject goDamageTrigger;
    [SerializeField] private GameObject goParticles;
    #endregion

    // ---------------------------------- Private ----------------------------------
    #region private
    private PlayerManager playerManager;
    private AudioManager audioManager;

    private LayerMask layerMask;

    private GameObject goTarget;
    private GameObject goFiredBy;

    private Vector3 v3StartPosition;
    private Vector3 v3PreviousePosition;
    private Vector3 v3NewPosition;
    private Vector3 v3TargetPosition;

    private float fStartTime;
    private float fTimeToTarget;
    private float fFrac;
    private float fExplosionVolume;

    private bool blnIsPlayerProjectile;
    private bool blnMove = false;

    #endregion


    // ################################## Awake / Start / Update ##################################
    // Called by Player
    public void SetupProjectile (Vector3 _v3StartPosition, Vector3 _v3TargetPosition) 
    {
        v3StartPosition = _v3StartPosition;

        v3TargetPosition = _v3TargetPosition;

        blnIsPlayerProjectile = true;

        fExplosionVolume = .75f;

        Setup();
    }

    // Called by Enemy
    public void SetupProjectile(Vector3 _v3StartPosition, GameObject _goTarget) 
    {
        v3StartPosition = _v3StartPosition;

        goTarget = _goTarget;

        v3TargetPosition = goTarget.transform.position;

        blnIsPlayerProjectile = false;

        fExplosionVolume = 1f;

        Setup();
    }

    private void Setup()
    {
        audioManager = InitialComponents.GetAudioManager();
        playerManager = InitialComponents.GetPlayerManager();
     
        fStartTime = Time.time;
        fTimeToTarget = (Vector3.Distance(transform.position, v3TargetPosition) + 2 * Vector3.Distance(transform.position, new Vector3(transform.position.x, transform.position.y + fShotHeight, transform.position.z))) / fSpeed;

        layerMask = (1 << 10); // Obstacles
        layerMask |= (1 << 11); // Player
        layerMask |= (1 << 13); // Floor
        layerMask |= (1 << 15); // Enemy

        blnMove = true;
    }


    void Update()
    {
        if (blnMove)
        {
            Move();
        }
    }

    // ################################## Move ##################################
    private void Move()
    {
        fFrac = (Time.time - fStartTime) / fTimeToTarget;

        if (fFrac > 1.2)
        {
            print("shouldn't happen");
            Hit();
            blnMove = false;
        }
        else
        {
            v3PreviousePosition = transform.position;

            float fYPos = v3StartPosition.y + acShotArc.Evaluate(fFrac) * fShotHeight;

            if (!blnIsPlayerProjectile)
            {
                v3TargetPosition = goTarget.transform.position;
            }

            v3NewPosition = Vector3.Lerp(v3StartPosition, v3TargetPosition, fFrac);

            v3NewPosition.y = fYPos;

            transform.position = v3NewPosition;

            CheckForHits();
        }
    }


    private void CheckForHits()
    {
        Ray rayTravledPath = new Ray(v3PreviousePosition, v3NewPosition - v3PreviousePosition);

        RaycastHit[] allRaycastHit = Physics.RaycastAll(rayTravledPath, Vector3.Distance(v3PreviousePosition, v3NewPosition),  layerMask);

        foreach (RaycastHit rayHit in allRaycastHit)
        {
            transform.position = rayHit.point;

            Hit();
       }      
    }


    // ################################## Projectile Hit ##################################
    private void Hit()
    {
        audioManager.PlayExplosionSound(fExplosionVolume);

        blnMove = false;

        goShell.SetActive(false);

        goParticles.SetActive(true);

        ExplosionDamage();

        if (!blnIsPlayerProjectile)
        {
            playerManager.KillPlayer(goTarget);
        }

        transform.parent = InitialComponents.GetProjectileDeadParent().transform;

        Destroy(gameObject, 1f);
    }


    private void ExplosionDamage()
    {
        float fRadius = goDamageTrigger.transform.localScale.x / 2;

        Collider[] colOverlap = Physics.OverlapSphere(transform.position, fRadius);

        foreach (Collider col in colOverlap)
        {
            if (col.gameObject.layer == 15) // Enemy
            {
                if (col.gameObject.transform.parent.GetComponent<EnemyHealth>() != null)
                {
                    col.gameObject.transform.parent.gameObject.GetComponent<EnemyHealth>().Kill();  //Turret
                }
                else
                {
                    col.gameObject.transform.parent.parent.gameObject.GetComponent<EnemyHealth>().Kill();  //Tank
                }          
            }

            if (col.gameObject.layer == 11) // Player
            {
                playerManager.KillPlayer(col.gameObject.transform.parent.gameObject);

                col.gameObject.transform.parent.GetComponent<PlayerHealth>().blnIsAirborne = true;

                Rigidbody rb = col.gameObject.transform.parent.GetComponent<Rigidbody>();

                rb.isKinematic = false;
                rb.constraints = RigidbodyConstraints.None;
                rb.useGravity = true;

                rb.angularDrag = 5;

                Vector3 v3ExplosionDirection = (col.gameObject.transform.parent.transform.position - transform.position).normalized;

                v3ExplosionDirection.y = 0;

                rb.AddForce((Vector3.up * fExplosionUpForceModifier + v3ExplosionDirection) * fExplosionForce, ForceMode.Impulse);
                rb.AddTorque(new Vector3(Random.Range(-1, 1), Random.Range(-1, 1), Random.Range(-1, 1)) * fExplosionTorque, ForceMode.Impulse);
            }
        }
    }
}