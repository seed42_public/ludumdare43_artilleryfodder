﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;

public class PlayerMovement : MonoBehaviour
{

    // ################################## Global Variables ##################################

    // ---------------------------------- Public ----------------------------------
    #region Serialized Private
     [Header("link")]
    [SerializeField] private GameObject goBottom;
    [SerializeField] private GameObject goTop;
    [SerializeField] private GameObject goTarget;
    [SerializeField] private GameObject goCannon;

    [SerializeField] private GameObject goProjectilePrefab;

    [Header("Mouse Over")]
    [SerializeField] private float fMaxCameraRayDistance;

    [Header("Player Parameters")]
    [SerializeField] private float fMoveSpeed;
    [SerializeField] private float fMinTargetRange;
    [SerializeField] private float fTargetRange;
    [SerializeField] private float fTimeBetweenShots;
    #endregion

    // ---------------------------------- Private ----------------------------------
    #region private
    private AudioManager audioManager;
    private PlayerManager playerManager;

    private Camera myMainCamera;

    private Rigidbody rb;

    private LayerMask mouseRotationLayerMask;

    private GameObject goProjectileParent;
    private GameObject goProjectile;

    private float fTimeLastShot;
    #endregion


    // ################################## Awake / Start / Update ##################################
    void Start()
    {
        audioManager = InitialComponents.GetAudioManager();
        playerManager = InitialComponents.GetPlayerManager();

        rb = GetComponent<Rigidbody>();

        myMainCamera = InitialComponents.GetMainCamera();

        mouseRotationLayerMask = 1 << 9;    // MouseRotationPlane
        mouseRotationLayerMask |= 1 << 10;  // Obstacles
        mouseRotationLayerMask |= 1 << 13;  // Floor
        mouseRotationLayerMask |= 1 << 15;  // Enemies

        goProjectileParent = InitialComponents.GetProjectileParent();

    }

    void Update()
    {
        Move();

        Rotate();

        if (Input.GetMouseButtonDown(0) && Time.time > fTimeLastShot + fTimeBetweenShots && !playerManager.blnGameOver)
        {
            if (EventSystem.current.IsPointerOverGameObject() && EventSystem.current.currentSelectedGameObject != null)
                return;

            Shoot();
        }

    }


    // ################################## Move / Rotate ##################################
    private void Move()
    {
        float fVerticalChange = Input.GetAxis("Vertical");

        float fHorizontalChange = Input.GetAxis("Horizontal");

        Vector3 v3Force = new Vector3(fHorizontalChange, 0, fVerticalChange) * fMoveSpeed * Time.deltaTime;

        rb.AddForce(v3Force, ForceMode.VelocityChange);

        goBottom.transform.LookAt(goBottom.transform.position + v3Force);
    }

    private void Rotate()
    {
        Ray rayFromCamera = myMainCamera.ScreenPointToRay(Input.mousePosition);

        RaycastHit raycastHit;

        if (EventSystem.current.IsPointerOverGameObject() && EventSystem.current.currentSelectedGameObject != null)
            return;

        if (Physics.Raycast(rayFromCamera, out raycastHit, fMaxCameraRayDistance, mouseRotationLayerMask))
        {
            Vector3 v3Direction = raycastHit.point - transform.position;

            float fYRotation = Mathf.Atan2(v3Direction.x, v3Direction.z) * 180 / Mathf.PI;

            Quaternion qNewRotation = Quaternion.Euler(0, FixRotation(fYRotation), 0);

            goTop.transform.rotation = qNewRotation;

            float fDistance = Mathf.Clamp(Vector3.Distance(transform.position, raycastHit.point), fMinTargetRange, fTargetRange);

            Vector3 v3TargetPosition = transform.position + qNewRotation * (Vector3.forward * fDistance);

            v3TargetPosition.y = raycastHit.point.y + .1f;

            goTarget.transform.position = v3TargetPosition;
        }
    }


    private float FixRotation(float fUnfixedRotation)
    {
        float fFixedRotation = fUnfixedRotation;

        while (fFixedRotation < 0)
        {
            fFixedRotation += 360;
        }
        while (fFixedRotation > 360)
        {
            fFixedRotation -= 360;
        }

        return fFixedRotation;
    }


    // ################################## Shoot ##################################
    private void Shoot()
    {
        audioManager.PlayShootingSound(1f);

        fTimeLastShot = Time.time;

        goProjectile = Instantiate(goProjectilePrefab, goCannon.transform.position, Quaternion.identity, goProjectileParent.transform);
        goProjectile.GetComponent<Projectile>().SetupProjectile(goCannon.transform.position, goTarget.transform.position);
    }
}