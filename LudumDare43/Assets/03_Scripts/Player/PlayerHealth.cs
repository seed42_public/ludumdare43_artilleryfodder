﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerHealth : MonoBehaviour {

    // ################################## Global Variables ##################################

    // ---------------------------------- Public ----------------------------------
    #region public
    [Header("dont change")]
    public bool blnIsAlive;
    public bool blnIsAirborne;
    #endregion

    #region Serialized private
    [Header("Threshholdes")]
    [SerializeField] private float fMoveThreshhold;
    [SerializeField] private float fRotateThreshhold;

    [Header("link")]
    [SerializeField] private GameObject goTop;
    [SerializeField] private GameObject goBottom;
    [SerializeField] private List<GameObject> lstWreckParticles = new List<GameObject>();

    [Header("Explosion Parameters")]
    [SerializeField] private float fExplosionForce;
    [SerializeField] private float fExplosionTorque;
    [SerializeField] private float fExplosionUpForceModifier;
    #endregion


    // ---------------------------------- Private ----------------------------------
    #region private
    private Vector3 v3PreviousePosition;
    private Quaternion qPreviouseRotation;
    #endregion


    // ################################## Awake / Start / Update ##################################
    private void Start()
    {
        blnIsAlive = true;
    }

    private void Update()
    {
        if (!blnIsAlive && blnIsAirborne)
        {
            CheckIfAirborne();
        }
    }


    // ################################## Check if hit by projectile - if not freez rigidbody ##################################
    private void CheckIfAirborne()
    {

        if (gameObject.GetComponent<Rigidbody>().velocity.sqrMagnitude < fMoveThreshhold && gameObject.GetComponent<Rigidbody>().angularVelocity.sqrMagnitude < fRotateThreshhold)
        {
            blnIsAirborne = false;

            gameObject.GetComponent<Rigidbody>().constraints = RigidbodyConstraints.FreezeAll;

            gameObject.GetComponent<Rigidbody>().velocity = Vector3.zero;

            gameObject.GetComponent<Rigidbody>().angularVelocity = Vector3.zero;

            gameObject.GetComponent<Rigidbody>().angularDrag = 25;

            gameObject.GetComponent<Rigidbody>().freezeRotation = true;
        }

    }


    // ################################## Destry when Corpse entery Spawn area ##################################
    private void OnTriggerEnter(Collider other)
    {
        OnTriggerStay(other);
    }

    private void OnTriggerStay(Collider other)
    {
        if (!blnIsAlive)
        {
            if (other.transform.gameObject.layer == 16) // Spawn
            {
                Destroy(goTop);
                Destroy(goBottom);

                foreach (GameObject goWreck in lstWreckParticles)
                {
                    goWreck.SetActive(true);

                    Rigidbody rb = goWreck.GetComponent<Rigidbody>();

                    if (rb != null)
                    {
                        rb.angularDrag = 5;

                        Vector3 v3ExplosionDirection = (gameObject.transform.parent.transform.position - rb.centerOfMass).normalized;

                        v3ExplosionDirection.y = 0;

                        rb.AddForce((Vector3.up * fExplosionUpForceModifier + v3ExplosionDirection) * fExplosionForce, ForceMode.Impulse);
                        rb.AddTorque(new Vector3(Random.Range(-1, 1), Random.Range(-1, 1), Random.Range(-1, 1)) * fExplosionTorque, ForceMode.Impulse);
                    }
                }
            }
        }
    }

}