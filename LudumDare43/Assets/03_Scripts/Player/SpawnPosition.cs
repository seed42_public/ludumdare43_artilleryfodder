﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SpawnPosition : MonoBehaviour {

    // ################################## Global Variables ##################################

    // ---------------------------------- Public ----------------------------------
    #region Serialized private
    [SerializeField] private float fOffset;
    #endregion


    // ################################## Awake / Start / Update ##################################



    // ################################## Variouse ##################################
    public Vector3 GetRandomSpawnPosition()
    {
        Vector3 v3SpawnPosition = transform.position;

        float fXSize = transform.localScale.x * 10 / 2;
        float fZSize = transform.localScale.z * 10 / 2;

        Vector3 randomDir = new Vector3(Random.Range(-fXSize + fOffset, fXSize - fOffset), 0, Random.Range(-fZSize + fOffset, fZSize - fOffset));

        randomDir = transform.rotation * randomDir;

        return v3SpawnPosition + randomDir;
    }

}