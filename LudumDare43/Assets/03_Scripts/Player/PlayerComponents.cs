﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerComponents : MonoBehaviour {

    // ################################## Global Variables ##################################

    // ---------------------------------- Public ----------------------------------
    #region public
    public GameObject goRayTarget;
    public GameObject goBottom;
    public GameObject goTop;
    public GameObject goTargetHelper;
    #endregion

}