﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LevelManager : MonoBehaviour {

    // ################################## Global Variables ##################################

    // ---------------------------------- Public ----------------------------------
    #region private Serialized
    [Header("public - don't set manually")]
    public List<GameObject> lstEnemies = new List<GameObject>();
    public List<GameObject> lstDestroyedEnemies = new List<GameObject>();
    #endregion

    #region Serialized private
    [Header("public - need to be set Manually!!!")]
    [SerializeField] private List<GameObject> lstOriginalEnemies = new List<GameObject>();
    public List<GameObject> lstSpawns = new List<GameObject>();
    #endregion


    // ################################## Awake / Start / Update ##################################
    private void Awake()
    {
        RevertLevel();
    }


    // ################################## Revert Level ##################################
    // Called when Game is restarted
    public void RevertLevel()
    {
        foreach (GameObject goEnemy in lstOriginalEnemies)
        {
            GameObject goClone = Instantiate(goEnemy, goEnemy.transform.position, goEnemy.transform.rotation, goEnemy.transform.parent);

            lstEnemies.Add(goClone);

            goClone.SetActive(true);

            goEnemy.SetActive(false);
        }

        foreach (GameObject goOldEnemy in lstDestroyedEnemies)
        {
            Destroy(goOldEnemy);
        }
    }
	

}