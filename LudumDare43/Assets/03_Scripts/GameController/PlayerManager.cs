﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class PlayerManager : MonoBehaviour {

    // ################################## Global Variables ##################################

    // ---------------------------------- Classes ----------------------------------
    #region classes

    #endregion

    // ---------------------------------- Public ----------------------------------

    #region public
    public bool blnGameOver = true;
    #endregion

    #region Serialized Private
    [Header("Levels")]
    [SerializeField] private List<GameObject> lstLevels = new List<GameObject>();

    [Header("Prefabs")]
    [SerializeField] private GameObject goPlayerPrefab;

    [Header("Container in Scene")]
    [SerializeField] private GameObject goPlayerParent;
    [SerializeField] private GameObject goProjectileParent;

    [Header("Materials")]
    [SerializeField] private Material playerDestroyedMaterial;
    [SerializeField] private PhysicMaterial slippery;

    [Header("GUI Elements")]
    [SerializeField] private GameObject goUILevelComplete;
    [SerializeField] private GameObject goUIStartScreen;
    [SerializeField] private GameObject goUIGameComplete;
    [SerializeField] private GameObject goUITut;
    [SerializeField] private GameObject goUIInGame;
    [SerializeField] private Text textCurrentLevel;
    [SerializeField] private Text textTotalLevels;
    [SerializeField] private Text textCasualties;
    [SerializeField] private Text textFinishCasualtiesCount;

    #endregion

    // ---------------------------------- Private ----------------------------------
    #region private 
    private List<GameObject> lstSpawn = new List<GameObject>();
    private List<GameObject> lstEnemies = new List<GameObject>();

    private GameObject goCurrentLevel;
    private GameObject goLivingPlayer;

    private int iDeathCounter;
    private int iCurrentLevel;

    public bool blnGameStarted = false;
    public bool blnLevelFinished = false;
    #endregion




    // ################################## Awake / Start / Update ##################################
    private void Update()
    {
        if (blnGameStarted)
        {
            CheckForVictory();
        }
    }

    // ################################## Start/Restart Level ##################################
    // Called by Button
    public void StartGame()
    {
        iCurrentLevel = 0;

        iDeathCounter = 0;

        StartNextLevel();

        goUIStartScreen.SetActive(false);

        goUIInGame.SetActive(true);

        textTotalLevels.text = "/ " + lstLevels.Count.ToString();

        blnGameStarted = true;
    }

    // Called by Button
    public void RestartGame()
    {
        foreach(GameObject goLevel in lstLevels)
        {
            goLevel.GetComponent<LevelManager>().RevertLevel();

            goLevel.SetActive(false);
        }
        iCurrentLevel = 0;

        iDeathCounter = 0;

        textCasualties.text = iDeathCounter.ToString();

        goUIGameComplete.SetActive(false);

        blnGameStarted = true;

        StartNextLevel();      
    }


    private void StartNextLevel()
    {
        if (goCurrentLevel != null)
        {
            goCurrentLevel.SetActive(false);
        }

        blnLevelFinished = false;

        blnGameOver = false;

        goCurrentLevel = lstLevels[iCurrentLevel];

        goCurrentLevel.SetActive(true);

        lstSpawn = goCurrentLevel.GetComponent<LevelManager>().lstSpawns;
        lstEnemies = goCurrentLevel.GetComponent<LevelManager>().lstEnemies;

        DestroyAllChildren(goPlayerParent);

        DestroyAllChildren(goProjectileParent);

        RespawnPlayer();

        goUILevelComplete.SetActive(false);

        textCurrentLevel.text = (iCurrentLevel+1).ToString();
    }


    public void CheckForVictory()
    {
        if (lstEnemies.Count < 1)
        {
            blnGameOver = true;

            if (goProjectileParent.transform.childCount < 1 && blnLevelFinished == false)
            {
                blnLevelFinished = true;

                iCurrentLevel++;

                if (iCurrentLevel >= lstLevels.Count)
                {
                    goUIGameComplete.SetActive(true);

                    textFinishCasualtiesCount.text = iDeathCounter.ToString();

                    blnGameStarted = false;
                }
                else
                {
                    goUILevelComplete.SetActive(true);
                }
            }
        }
    }


    // ################################## Player ##################################
    public void SetLivingPlayer(GameObject goPlayer)
    {
        goLivingPlayer = goPlayer;
    }


	public GameObject GetLivingPlayer()
    {
        return goLivingPlayer;
    }

    public void KillPlayer(GameObject goPlayer)
    {

        if (goPlayer == goLivingPlayer)
        {
            PlayerComponents playerComponents = goLivingPlayer.GetComponent<PlayerComponents>();

            playerComponents.goBottom.GetComponent<Renderer>().material = playerDestroyedMaterial;

            playerComponents.goTop.GetComponent<Renderer>().material = playerDestroyedMaterial;

            goLivingPlayer.GetComponent<PlayerHealth>().blnIsAlive = false;

            goLivingPlayer.GetComponent<Rigidbody>().isKinematic = true;

            goLivingPlayer.GetComponent<PlayerComponents>().goBottom.GetComponent<BoxCollider>().material = slippery;

            // Destroy everything that makes the player controllable
            Destroy(goLivingPlayer.GetComponent<PlayerMovement>());

            Destroy(playerComponents.goTargetHelper);

            Destroy(playerComponents);

            SetLivingPlayer(null);

            iDeathCounter++;

            RespawnPlayer();

            textCasualties.text = iDeathCounter.ToString();
        }

    }


    public void RespawnPlayer()
    {
        int iSpawnIndex = iDeathCounter % lstSpawn.Count;

        Vector3 v3SpawnPosition = lstSpawn[iSpawnIndex].GetComponent<SpawnPosition>().GetRandomSpawnPosition();

        goLivingPlayer = Instantiate(goPlayerPrefab, v3SpawnPosition, Quaternion.identity, goPlayerParent.transform);

        foreach (GameObject goEnemy in lstEnemies)
        {
            goEnemy.GetComponent<EnemyAim>().SetTarget(goLivingPlayer);
        }
    }

    // ################################## Turret ##################################
    public void RemoveTurret(GameObject goTurret)
    {
        lstEnemies.Remove(goTurret);
        goCurrentLevel.GetComponent<LevelManager>().lstDestroyedEnemies.Add(goTurret);
    }


    // ################################## Var ##################################
    private void DestroyAllChildren(GameObject goParent)
    {
        foreach (Transform child in goParent.transform)
            Destroy(child.gameObject);
    }

    public void ShowTutorial()
    {
        goUITut.SetActive(true);
    }

    public void HideTutorial()
    {
        goUITut.SetActive(false);
    }

}