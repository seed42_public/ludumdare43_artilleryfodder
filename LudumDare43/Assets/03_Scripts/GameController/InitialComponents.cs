﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class InitialComponents : MonoBehaviour {

    // ################################## Global Variables ##################################

    // ---------------------------------- Classes ----------------------------------
    #region classes

    #endregion

    #region Serialized Privates
    [SerializeField] private Camera mainCamera;

    [SerializeField] private GameObject goProjectilesParent;
    [SerializeField] private GameObject goProjectilesDeadParent;

    [SerializeField] private PlayerManager playerManager;
    [SerializeField] private AudioManager audioManager;
    #endregion

    // ---------------------------------- Private ----------------------------------
    #region private
    private static Camera staticMainCamera;

    private static GameObject staticProjectilesParent;
    private static GameObject staticProjectilesDeadParent;

    private static PlayerManager staticPlayerManager;
    private static AudioManager staticAudioManager;
    #endregion


    // ################################## Awake / Start / Update ##################################
    void Awake()
    {
        SetMainCameraStatic();
    }

    private void SetMainCameraStatic()
    {
        staticMainCamera = mainCamera;

        staticPlayerManager = playerManager;

        staticProjectilesParent = goProjectilesParent;

        staticProjectilesDeadParent = goProjectilesDeadParent;

        staticAudioManager = audioManager;
    }


    // ################################## Variouse ##################################

    public static Camera GetMainCamera() { return staticMainCamera; }

    public static PlayerManager GetPlayerManager() { return staticPlayerManager; }

    public static GameObject GetProjectileParent() { return staticProjectilesParent; }

    public static GameObject GetProjectileDeadParent() { return staticProjectilesDeadParent; }

    public static AudioManager GetAudioManager() { return staticAudioManager; }
}