﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AudioManager : MonoBehaviour {

// ################################## Global Variables ##################################

	// ---------------------------------- Public ----------------------------------
	#region Serialized private
    [SerializeField] private List<AudioClip> lstExplosions = new List<AudioClip>();
    [SerializeField] private List<AudioClip> lstShoots = new List<AudioClip>();
    [SerializeField] private List<GameObject> lstAudioSources = new List<GameObject>();
    #endregion

    // ---------------------------------- Private ----------------------------------
    #region private
    private List<GameObject> lstPlayingSources = new List<GameObject>();
    private Queue<GameObject> queueAudioSources = new Queue<GameObject>();
    #endregion


    // ################################## Awake / Start / Update ##################################
    void Start ()
    {
		foreach (GameObject go in lstAudioSources)
        {
            queueAudioSources.Enqueue(go);
        }
	}
	
	void Update ()
    {		
        for (int i = 0; i < lstPlayingSources.Count; i++)
        {
            if (!lstPlayingSources[i].GetComponent<AudioSource>().isPlaying)
            {
                queueAudioSources.Enqueue(lstPlayingSources[i]);
                lstPlayingSources.Remove(lstPlayingSources[i]);
                i--;
            }
        }

	}
	
	
	// ################################## Variouse ##################################
	
    public void PlayExplosionSound(float fVolume)
    {
        if (queueAudioSources.Count < 1)
        {
            print("no availible audosource");
            return;
        }

        PlaySound(lstExplosions[Random.Range(0, lstExplosions.Count)], fVolume);
    }

    public void PlayShootingSound(float fVolume)
    {
        if (queueAudioSources.Count < 1)
        {
            print("no availible audosource");
            return;
        }

        PlaySound(lstShoots[Random.Range(0, lstShoots.Count)], fVolume);
    }


    private void PlaySound(AudioClip currentClip, float fVolume)
    {
        GameObject goCurrentSource = queueAudioSources.Dequeue();

        AudioSource audioSource = goCurrentSource.GetComponent<AudioSource>();

        audioSource.volume = fVolume;

        audioSource.clip = lstExplosions[Random.Range(0, lstExplosions.Count)];

        audioSource.Play();

        lstPlayingSources.Add(goCurrentSource);
    }

}